# frozen_string_literal: true

module ResearchableRubyStyleguide
  # Include global rake tasks
  # @author Researchable
  class Railtie < Rails::Railtie
    rake_tasks do
      load 'tasks/style.rake'
    end
  end
end

