# frozen_string_literal: true

require 'tmpdir'
require 'securerandom'

namespace :style do
  desc 'Update the styles to the last version'
  task :update do # rubocop:disable Rails/RakeEnvironment
    puts 'Updating style'
    system('bundle update researchable_ruby_style')
    system('bundle exec rake style:inject_styles')
  end

  task :inject_styles do # rubocop:disable Rails/RakeEnvironment
    def add_file(output_path, file_name)
      File.open('.rubocop.yml', 'a') do |file| 
        file.puts('inherit_from:')
        file.puts("- #{File.join(outputpath, filename}")
      end
      full_gem_path = Gem.loaded_specs['researchable_ruby_style'].full_gem_path
      FileUtils.cp File.join(full_gem_path, 'style', filename), output_path
    end

    def cleanup(output_path)
      FileUtils.rm_rf('.rubocop.yml')
      FileUtils.rm_rf(output_path)
      FileUtils.mkdir_p(output_path)
    end
    output_path = File.join('./style', 'ruby')

    cleanup(output_path)

    add_file(output_path, 'rubocop.yml')
    add_file(output_path, 'rubocop-todo.yml')
  end
end

