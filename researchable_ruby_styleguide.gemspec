require_relative 'lib/researchable_ruby_styleguide/version'

Gem::Specification.new do |spec|
  spec.name          = "researchable_ruby_styleguide"
  spec.version       = ResearchableRubyStyleguide::VERSION
  spec.authors       = ["Researchable"]
  spec.email         = ["gems@researchable.nl"]

  spec.summary       = 'Styleguide for Researchable Ruby projects'
  spec.description   = 'Styleguide for Researchable Ruby projects'
  spec.homepage      = "https://researchable.nl"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/researchable/general/styles/rubocop"
  spec.metadata["changelog_uri"] = "https://gitlab.com/researchable/general/styles/rubocop"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  s.add_runtime_dependency 'rubocop', ["~> 0.90"]
  s.add_runtime_dependency 'rubocop-rails'
  s.add_runtime_dependency 'rubocop-performance', ['~> 1.6.1']
  s.add_runtime_dependency 'rubocop-rspec', ['~> 1.40.0']
end
